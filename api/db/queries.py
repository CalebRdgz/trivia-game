from fastapi import APIRouter
from db.connection import query, extract

router = APIRouter()

def prefix(str):
    return f"api/{str}"

@router.get(prefix('categories'))
def categories(page: int = 0):
    return [result_arr[0] for result_arr in queries.get_categories(page)]