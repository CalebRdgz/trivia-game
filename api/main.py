from fastapi import FastAPI
from pydantic import BaseModel
import psycopg

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/api/categories")
def get_categories_first_100(page: int = 0):
    with psycopg.connect('dbname=trivia-game user=trivia-game') as conn:
            with conn.curosr() as cur:

                cur.execute("""
                    SELECT is, title, canon
                    FROM categories
                    ORDER BY title
                    LIMIT 100 OFFSET %s
                """,(page*100,))
                print('just did execute')

                results = []
                for row in cur.fetchall():
                    record = {}
                    print(row)
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results


@app.get('/categories')
def read_categories(category_id: int, title: str, canon: bool):
    print('main function')
    return {'category_id': category_id, 'title': title, 'canon': canon}