from fastapi import APIRouter
from pydantic import BaseModel

from db import queries

router = APIRouter()

def prefix(str):
    return f"api/{str}"

## GET LIST
@router.get(prefix('categories'))
async def categories(page: int = 0):
    return [x for xs in queries.get_categories(page) for x in xs]

##Simple model for body inputs
class Category(BaseModel):
    title: str

##POST
@router.post(prefix('categories'))
async def create_category(cat: Category):
    return queries.add_category(cat.title)[0][0]

##PUT
@router.put(prefix('categories/{category_id}'))
async def put_category(cat: Category):
    return queries.add_category(cat.title)[0][0]

##DELETE
@router.delete(prefix('categories/{category_id}'))
async def delete_category(cat: Category):
    return queries.add_category(cat.title)[0][0]